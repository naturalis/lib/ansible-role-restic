# ansible-role-restic

## Description

Deploy and manage [restic](https://restic.net/) using systemd to start and
schedule restic services. Pre- and post scripts are not managed by this role,
set those up in your application codebase.

## Requirements

- Ansible >= 2.8
- bzip2 installed on deployer machine (same one where ansible is installed)
- Ubuntu 20.04 (systemd version 244 or higher). Restart=on-failure [is not allowed for Type=oneshot](https://github.com/systemd/systemd/issues/2582) in older versions. Type oneshot is needed to run services in order using systemd target files.

## Role Variables

All variables which can be overridden are stored in 
[defaults/main.yml](defaults/main.yml) and [vars/main.yml](vars/main.yml). 
By default this role does the following:

- Create systemd services to start restic
- Create systemd timers to schedule backups
- Backup `/data/backup` every day at 3 a.m. 
- Run a restic forget action every day at 10 a.m.
- Create a textfile collector file every 4 hours for use with Prometheus node exporter

To install and configure restic, but without any scheduled jobs set:

```
restic_jobs: []
restic_stats_schedule: ''
```

This can be usefull on test and development server. 

## Performance 
By default no resource limitations are set on the services. They can be implemented with _service_opts_:
```
restic_jobs:
  - name: backup
    command: 'backup --files-from {{ restic_config_path }}/restic_includes --exclude-file {{ restic_config_path }}/restic_excludes'
    timer: "{{ restic_backup_schedule }}"
    service_opts: |
      CPUWeight=30
      CPUQuota=30%
```

## Security

To ensure high security this role can allow restic to be run as different user
than root and still allowing read-only access to files. This is implemented by
following [PR#1483](https://github.com/restic/restic/pull/1483) from restic
repository.

If you need to run certain tools as another user, make sure to list those in
`restic_sudo_command_whitelist` as follows:

```yaml
restic_sudo_command_whitelist:
   - command: /usr/bin/some_backup_related_command_that_needs_sudo
     runas: root
```

Then, in your actual backup command, add the command as `sudo -u root
/usr/bin/some_backup_related_command_that_needs_sudo`.

## Application backup example

In this example the postgres backup script will run before the restic backup:

```mermaid
flowchart LR
    subgraph sg1[.]
    a[restic-backup.timer<br/>OnCalendar=*-*-* 3:00:00<br/>Unit=restic-backup.target]
    end
    subgraph sg2[.]
    b[restic-backup.target]
    c[# Not managed by this role<br/>restic-backup-postgres.service<br/>Before: restic-backup.service<br/>WantedBy: restic-backup.target]
    d[restic-backup.service<br/>WantedBy: restic-backup.target]
    b --- c
    b --- d
    end
    sg1 --> sg2
```

Create service file `restic-backup-postgres.service` in `/etc/systemd/system`:

```ini
[Unit]
Description=Create postgres dumps
Before=restic-backup.service

[Service]
Type=oneshot
ExecStart=/usr/local/bin/backup_postgres.sh

[Install]
WantedBy=restic-backup.target
```

Create script `/usr/local/bin/backup_postgres.sh`:

```sh
#!/usr/bin/env bash
set -euo pipefail

sudo -u postgres pg_dump -f /backups/postgres.dump
```

Enable the newly created service and start the application- and restic backup:

```sh
systemctl enable restic-backup-postgres.service
systemctl start restic-backup.target
```

## Application restore example

```mermaid
flowchart LR
    subgraph sg1[.]
    a[restic-restore.timer<br/>OnCalendar=*-*-* 00/4:00]
    end

    subgraph sg2[.]
    b[restic-restore.target]
    c[# Not managed by this role<br/>restic-restore-postgres.service<br/>After: restic-restore.service<br/>WantedBy: restic-backup.target]
    d[restic-restore.service<br/>WantedBy: restic-restore.target]
    b --- d
    b --- c
    end
    sg1 --> sg2
```

Create service file `restic-restore-postgres.service` in `/etc/systemd/system`:

```ini
[Unit]
Description=Run pg_restore and other post restic restore steps
After=restic-restore.service

[Service]
Type=oneshot
ExecStart=/usr/local/bin/restore_postgres.sh

[Install]
WantedBy=restic-restore.target
```

Enable the service and start the restore:

```sh
systemctl enable restic-restore-postgres.service
systemctl start restic-restore.target
```

## Restic commands

List last snapshots:

```sh
restic --no-lock --json snapshots --host $(hostname) --last | jq '.[0] | {paths: .paths, timestamp: .time}'
```

List unique paths and counts:

```sh
restic --no-lock --json --host $(hostname) snapshots | jq 'group_by(.paths) | map({"paths": .[0].paths, "total":length})'
```

Max unique paths count:

```sh
restic --no-lock --json --host $(hostname) snapshots | jq 'group_by(.paths) | map({"paths": .[0].paths, "total":length}) | max_by(.total) | .total'
```

## Stats

A gather stats script runs every 4 hours, triggerd by a systemd timer unit.

```mermaid
flowchart LR
    a[restic-stats.timer<br/>OnCalendar=*-*-* 00/4:00]
    b[restic-stats.service<br/>ExecStart=restic_textfile_collector.sh]
    c["/var/lib/node_exporter/restic.prom:<br/># HELP restic_unique_paths Unique paths in backup.<br/># TYPE restic_unique_paths gauge<br/>restic_unique_paths 1"]
    d["/etc/ansible/facts.d/restic_stats.fact:<br/>{'snapshot_count': 3,'unique_paths': 1}"]
    a --> b
    b --> c
    b --> d
```

### Playbook

Use it in a playbook as follows:

```yaml
---
- name: Install and manage Restic
  hosts: all
  become: true
  tasks:
    - include_role:
        name: restic
        apply:
          tags: restic
      tags: always
...
```

## Local Testing

```sh
pip3 install molecule
pip3 install molecule-docker
```

The preferred way of locally testing the role is to use Docker and
[molecule](https://github.com/ansible-community/molecule) (v3.x). You will have
to install Docker on your system. See Get started for a Docker package suitable
to for your system.

All packages you need to can be specified in one line:

```sh
pip install -rtest-requirements.txt
```

This should be similar to one listed in `.travis.yml` file in `install` section.
After installing test suit you can run test by running:

```sh
molecule test
```

For more information about molecule go to their
[docs](http://molecule.readthedocs.io/en/latest/).

## License

Apache License 2.0
